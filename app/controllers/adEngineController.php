<?php

use Illuminate\Routing\Controller;
use Illuminate\Database\Capsule\Manager as Capsule;


class adEngineController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| adEngine()
	|--------------------------------------------------------------------------
	| Child iframe
	|
	| Filter actions based on targeting criteria and redirect user
	|
	*/

	public function adEngine($randomaddeliverystring,$hash){

		$data = unserialize(crypto::decUrlParam($hash));

		/*
		|--------------------------------------------------------------------------
		| Timestamp check - Show "Expired" if accessing child iframe directly after 15secs
		|--------------------------------------------------------------------------
		*/

		//if ($data->timestamp < strtotime(gmdate('r', time()))){
			// echo 'Expired';
			 //exit;

		//}

		$request = RequestInfo::get($data->request_id);

		$campaign = $request->campaign;

		/*
		|--------------------------------------------------------------------------
		| Get Mobile device info array (version_id, device_id and mysql string to query)
		|--------------------------------------------------------------------------
		*/

		$deviceinfo = Helpers::getDeviceInfo();

		/*
		|--------------------------------------------------------------------------
		| Geo Block Filter - Get action id list (string) if this user is geo blocked
		| ($data->geo_block_traffic = true user does not match campaign geo and not
		| on exclude geo list)
		|--------------------------------------------------------------------------
		*/

		if($request->error_flag == 'GEO_BLOCK'){

			$actions = actionFilter::geoBlockFilter();

		}

		/*
		|--------------------------------------------------------------------------
		| Master Group Filter - Get action id list (string) if there is a master group
		| enabled for this campaign and geo; if not it will return campaign assigned actions
		|--------------------------------------------------------------------------
		*/

		else {

			$actions = actionFilter::masterGroupFilter($request->user_country, $campaign->campaign_id);

		}

		/*
		|--------------------------------------------------------------------------
		| "Visited" Filter - Get action list (array) from memcache that this user has visited
		|
		| If we find any actions we exclude from $actions array that we will
		| pass to geoIspCityFilter()
		|--------------------------------------------------------------------------
		*/

		$actions =  actionFilter::getVisited(explode(",",$actions));

		/*
		|--------------------------------------------------------------------------
		| Geo ISP City Filter - Get actions that have passed teh filter chain:
		| ISP and City, if no match, we try for ISP match
		| If no match we try for actions set as ALL ISPS
		| If no match, then return empty array (which will return a banner
		|--------------------------------------------------------------------------
		*/

		$actions =  actionFilter::geoIspCityFilter($actions, $request->user_country, $request->city_state, $request->isp_name, $deviceinfo);

		/*
		|--------------------------------------------------------------------------
		| If result is empty, user has seen all these actions based on capping
		| Write to ES/DB and show banner (set error flag)
		| If traffic was geo_block and we didnt find any actions matching user
		| user geo, we set error_flag to GEO_BLOCK and es object field to true
		|--------------------------------------------------------------------------
		*/

		if(empty($actions)){

			if($request->error_flag == 'GEO_BLOCK'){

				$request->geo_block = true;

			}else{

				$request->error_flag = 'ACTION_EMPTY';
				$request->no_action = true;

			}

			statsWrite::es($request);
			$request->delete();
			return render::generateBannerAd($campaign,$request->error_flag);
		}


		/*
		|--------------------------------------------------------------------------
		| Filters done, lets pick a random action from the filtered array of actions
		| If campaign has action_priority enabled, we will select a random action_id
		| from mobile_actions_priority BUT with a ORDER BY RAND() * (1000 - priority)
		| We then query mobile_actions table to get action data
		|--------------------------------------------------------------------------
		*/

		if($request->campaign->action_priority == 0){

			$actionPriority = MobileActionPriority::whereIn('action_id',$actions)
			->where('campaign_id',$campaign->campaign_id)
			//->orderByRaw("-LOG(RAND()) / priority")
			->orderByRaw("RAND() * (10 - priority)")
			->take(1)
			->get();

			$action = MobileAction::where('action_id',$actionPriority[0]->action_id)->get();


		}else{

			$action = MobileAction::whereIn('action_id',$actions)
			->orderByRaw("RAND()")
			->take(1)
			->get();

		}

		/*
		|--------------------------------------------------------------------------
		| If random selected action to redirect has cookie capping we set it on
		|  memcache with cookie_time setting (24/48/72h)
		|--------------------------------------------------------------------------
		*/

		if($action[0]->cookie_time != 0){
			actionFilter::setVisited($action);
		}

		/*
		|--------------------------------------------------------------------------
		| Prepare Tracking Link to redirect (postback append + random ref)
		| Create $data object containing action data, $data coming
		| from hash and device/version id (postback)
		| We also use this object to send data along the tracking link
		| as well as ES and DB (referer_raw) (optional?)
		|--------------------------------------------------------------------------
		*/

		$request->action_id = $action[0]->action_id;
		$request->action_code = $action[0]->action_code;
		$request->action_subid = $action[0]->action_subid;
		$request->network_id = $action[0]->network_id;
		$request->random_ref = $action[0]->random_ref;
		$request->device_id = $deviceinfo->device_id;
		$request->version_id = $deviceinfo->version_id;
		$request->redirect = true;


		$trackingLink = Helpers::prepareTrackingUrl($request);

		/*
		|--------------------------------------------------------------------------
		| Write data to es/db
		|--------------------------------------------------------------------------
		*/

		statsWrite::es($request);

		/*
		|--------------------------------------------------------------------------
		| Destroy Session and finally redirect / popup user :-)
		|--------------------------------------------------------------------------
		*/

		$request->delete();

		if($action[0]->action_type == 'popup'){

			render::generatePopupCode($trackingLink,$action[0]->popup_message);

		}
		
		//exit;

		render::generateRedirectCode($trackingLink);

	}


}
