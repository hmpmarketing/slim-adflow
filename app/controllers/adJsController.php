<?php

namespace App\Controllers;

use Illuminate\Routing\Controller;
use Adtree\request\RequestInfo;
use Adtree\campaign\campaignFilter;
use Adtree\encoder\crypto;
use Adtree\geo\GeoIP;
use Adtree\stats\statsWrite;
use Adtree\render\render;
use Adtree\unique\uniqueCheck;
use Campaign;
use stdClass;

class adJsController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| jsDefault() and jsCustomPath()
	|--------------------------------------------------------------------------
	|
	| Extends the method js() to support use or no use of custom paths
	| as laravel does not support optional first parameter for a route as dynamic
	*/

	public function jsDefault($randomjsstring,$hash){
		$this->js($hash);
	}

	public function jsCustomPath($custompath = null,$randomjsstring,$hash){
		$this->js($hash);
	}


	/*
	|--------------------------------------------------------------------------
	| js()
	|--------------------------------------------------------------------------
	|
	| Generates js adcode
	|
	*/

	public function js($hash){

		$campaign_id = crypto::decUrlParam($hash)[0];

		/*
		|--------------------------------------------------------------------------
		| Get campaign and user geo info
		|--------------------------------------------------------------------------
		*/

		$userGeoInfo = GeoIP::userGeoInfo();
		$campaign = Campaign::find($campaign_id);

		$request = new RequestInfo($campaign, $userGeoInfo);

		/*
		|--------------------------------------------------------------------------
		| Geo and Ip Filter checks
		| If traffic is desktop, we either generate a postview banner (campaign is
		| a cookie campaign) or we simply generate a banner
		|--------------------------------------------------------------------------
		*/

		if(!$request->filter()) {

			statsWrite::es($request);

			//If user is a desktop user and campaign is a pv campaign enabled, load pv
			if ($request->error_flag == 'POSTVIEW'){
				return render::generateJSPostView($campaign);
			}
			else{
				return render::generateJSBannerAd($campaign,$request->error_flag);
			}

		}

		/*
		|--------------------------------------------------------------------------
		| Filters passed? save request, gather request id to hash
		| and render adDeliveryController Iframe
		|--------------------------------------------------------------------------
		*/

		if(!$request->save()){
			Log::info("COULDN'T ADD KEY TO MEMCACHE: ". $request->id);
	    #Log::info("SERVER CONTENTS :". serialize($_SERVER));
	    #Log::info("REQUEST CONTENTS :". serialize($request));
		}

		$hashdata = new stdClass;
		$hashdata->timestamp = strtotime(gmdate('r', time())) + 10;
		$hashdata->request_id = $request->id;
		$hashdata = serialize($hashdata);
		$hash = crypto::encUrlParam($hashdata);

		return render::generateJSAdDeliveryIframe($campaign,$hash);

	}

}
