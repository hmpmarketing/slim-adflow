<?php

use Illuminate\Routing\Controller;
use Illuminate\Database\Capsule\Manager as Capsule;

class adDeliveryController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| adDeliveryDefault() and adDeliveryCustomPath()
	|--------------------------------------------------------------------------
	|
	| Extends the method adDelivery() to support use or no use of custom paths
	| as laravel does not support optional first parameter for a route as dynamic
	*/

	public function adDeliveryDefault($randomaddeliverystring, $hash, $zone_id = '_ZONEID_'){
		$this->adDelivery($hash,$zone_id);
	}

	public function adDeliveryCustomPath($custompath = null,$randomaddeliverystring, $hash, $zone_id = '_ZONEID_'){
		$this->adDelivery($hash,$zone_id);
	}


	/*
	|--------------------------------------------------------------------------
	| adDelivery()
	|--------------------------------------------------------------------------
	|
	| Generates topmost iframe when using iframe or .js adcode
	|
	*/

  /**
  * @param $hash
  * @param $zone_id
  */
  public function adDelivery($hash,$zone_id){

  	$decrypted_hash = crypto::decUrlParam($hash);

  	// DIRECT REQUEST TO AD DELIVERY
		if(is_numeric($decrypted_hash[0])){

			/*
			|--------------------------------------------------------------------------
			| Get campaign and user geo info
			|--------------------------------------------------------------------------
			*/
			$campaign_id = $decrypted_hash[0];

			$userGeoInfo = GeoIP::userGeoInfo();
			$campaign = Campaign::find($campaign_id);

			$request = new RequestInfo($campaign, $userGeoInfo);
			//print_r($request);exit;

			if(!$request->filter()) {

				statsWrite::es($request);

				//If user is a desktop user and campaign is a pv campaign enabled, load pv
				if ($request->error_flag == 'POSTVIEW'){
					return render::generatePostView($campaign);
				}
				else{
					return render::generateBannerAd($campaign,$request->error_flag);
				}

			}

			// Filters passed, save request
			$request->save();

		}
		// PASSED THROUGH JS
		else {

			$data = unserialize($decrypted_hash);

			/*
			|--------------------------------------------------------------------------
			| Timestamp check - Show "Expired" if accessing child iframe directly after 15secs
			|--------------------------------------------------------------------------
			*/

			if ($data->timestamp < strtotime(gmdate('r', time()))){
				 echo 'Expired';
				 exit;
			}

			$request = RequestInfo::get($data->request_id);
			if(!$request) {
				Log::info("COULDN'T GET KEY FROM MEMCACHE: ". $data->request_id);
			// 	Log::info("SERVER CONTENTS: ". serialize($_SERVER));
			}

		}

		/*
		|--------------------------------------------------------------------------
		| Filters passed? gather data to hash and render adEngineController Iframe
		| Set timestamp to 15secs ahead so that adEngine is "protected" if anyone
		| tries to load child iframe on its own, will show "Expired"
		|--------------------------------------------------------------------------
		*/

		$hashdata = new stdClass;
		$hashdata->timestamp = strtotime(gmdate('r', time())) + 10;
		$hashdata->request_id = $request->id;
		$hashdata = serialize($hashdata);
		$hash = crypto::encUrlParam($hashdata);

		return render::generateAdEngineIframe($request->campaign,$hash);


	}

}
