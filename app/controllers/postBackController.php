<?php

use Illuminate\Routing\Controller;
use Illuminate\Database\Capsule\Manager as Capsule;

class postBackController extends Controller {


	/*
	|--------------------------------------------------------------------------
	| postback()
	|--------------------------------------------------------------------------
	|
	| Calls updateDB() and when done, calls updateES()
	| 
	*/

	public function postback($nid= null,$data = null){
		
		if($_GET['data']){
		
			$data = $_GET['data'];
		}
		
		$db = $this->updateDB($data);
				
	}
	

	/*
	|--------------------------------------------------------------------------
	| updateDB()
	|--------------------------------------------------------------------------
	|
	| Creates a lead record on mobile_leads table indicating a conversion
	| 
	*/	
	
	public function updateDB($data){
				
		$decdata = base64_decode($data);
		$data = unserialize($decdata);
								
		//New Hash format
		if($data[9]){
		
			$ipaddress = long2ip($data[9]);
			
			$userGeoinfo = GeoIP::userGeoInfo($ipaddress);	
						
			//Country
			$usercountry = $userGeoinfo->isoCode;	

			//Remove whitespace from ISP Name for future comparison purposes
			$userisp = preg_replace('/\s+/', '', $userGeoinfo->isp);
			
			//City
			$usercity = $userGeoinfo->city_state;
			$usergeo_id = $userGeoinfo->geo_name_id;
			
			//Subid
			if($data[10]){
			
				$subid = $data[10];	
				
			}else{
				
				$subid = '';
				
			}
		
		//Old Hash
		}else{
			
			$usercity = '';	
			$usercountry = @$data[5];
			$userisp = @$data[6];
			$userisp = preg_replace('/\s+/', '', $userisp);
			
			
		}
		
		// retrieve unique id. set it to NULL if it's not set
		$uniqid = isset($data[11]) ? $data[11] : "NULL";
		
		//Lookup Device
		$queryDevice = Device::where('id',$data[7])->get();
		
		$data = array(
				'uniqid' => $uniqid,
				'campaign_id' => $data[1],
				'action_id' => $data[2],
				'network_id' => $data[3],
				'zone_id' => $data[4],
				'user_country' => $usercountry,
				'user_city' => $usercity,
				'geo_id' => $usergeo_id,
				'subid' => $subid,
				'isp_name' => $userisp,
				'device_id' => $queryDevice[0]->device_name,
				'version_id' => $data[8],
				'timestamp' => time(),
				);
				
				//print_r($data);exit;
		
		
		$lead = new MobileLead();
		$lead->uniqid = $data['uniqid'];
		$lead->campaign_id = $data['campaign_id'];
		$lead->action_id = $data['action_id'];
		$lead->network_id = $data['network_id'];
		$lead->zone_id = $data['zone_id'];
		$lead->user_country = $data['user_country'];
		$lead->user_city = $data['user_city'];
		$lead->user_ip = $ipaddress;
		$lead->geo_id = $data['geo_id'];
		$lead->subid = $data['subid'];
		$lead->isp_name = $data['isp_name'];
		$lead->os = $data['device_id'];
		$lead->os_version = $data['version_id'];
		$lead->timestamp = $data['timestamp'];
		$lead->date = date('Y-m-d H:i:s');
		$lead->save();
		
		
		//Update ES index
		$this->updateES($uniqid);
		
	}


	/*
	|--------------------------------------------------------------------------
	| updateES()
	|--------------------------------------------------------------------------
	|
	| Updates a document on ES setting converted to true by uniqid
	| 
	*/

	public function updateES($uniqid){
				
	  // tries to connect on ES and get all indexes. 
	  // Returns false in case it fails to do so
	  try {
		  
		$params = array();
		$params['hosts'] = array ('es-elb.adserver3.com');
		$client = new \Elasticsearch\Client($params);
		$indices = array_keys($client->indices()->getAliases());
				
	  }catch (Exception $e) {  
	      
			return false;
	  }
	  // sorts the indexes in descending order so we start
	  // looking for the doc on most recent indices
	  arsort($indices);
	  	
	  foreach ($indices as $index) {
		  		
		// work on *adserver3-stats indexes only
		if (strpos($index, 'adserver3-stats')) {
	
		  try {
			  
			  $updateParams['index']          = $index;
			  $updateParams['type']           = 'adserver3-stats';
			  $updateParams['id']             = $uniqid;
			  $updateParams['body']['doc']    = array('converted' => true, 'conversion_timestamp' => time());
			  			  		  
			  $client->update($updateParams);			           
			  
			  // returns true if it manages to update the document          
			  return true;      
		  }
		  catch (Exception $e) {      
			continue;
		  }
	   
		}  
	  }		
		
	}

}
