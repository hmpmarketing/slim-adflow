<?php

use Jenssegers\Agent\Agent;

/*
|--------------------------------------------------------------------------
| class campaignFilter
|--------------------------------------------------------------------------
|
| Geo and IP filter checks for a campaign
*/

class campaignFilter {

	/*
	|--------------------------------------------------------------------------
	| isUserGeoAllowed()
	|--------------------------------------------------------------------------
	|
	| Check if user country matches campaign geo(s)
	*/

    public static function isUserGeoAllowed($user_country,$campaign) {

		if((in_array($user_country, explode(",",$campaign->geo_countries)))){

			return true;

		}

	}

	/*
	|--------------------------------------------------------------------------
	| isUserOnGeoExclude()
	|--------------------------------------------------------------------------
	|
	| Check if user country is on geo_exclude list
	*/

    public static function isUserOnGeoExclude($user_country,$campaign) {

		if((in_array($user_country, explode(",",$campaign->geo_exclude)))){

			return true;

		}

	}

	/*
	|--------------------------------------------------------------------------
	| isUserIspWhitelisted()
	|--------------------------------------------------------------------------
	|
	| Check is user Isp is on whitelist (standard or hardcore) for campaign
	*/

    public static function isUserIspWhitelisted($user_country,$user_isp,$campaign) {

		if($campaign->isp_whitelist_type == 'isp_whitelist'){

			$isps = IspWhitelist::where('iso_code',$user_country)->get(array('isp_name'));

		}

		if($campaign->isp_whitelist_type == 'isp_whitelist_hardcore'){

			$isps = IspWhitelistHardcore::where('iso_code',$user_country)->get(array('isp_name'));

		}

		$whitelist_isps = array();
		foreach($isps as $isp){

			$whitelist_isps[] = $isp->isp_name;

		}

		if((in_array($user_isp, $whitelist_isps))){

			return true;

		}

	}

	/*
	|--------------------------------------------------------------------------
	| isUserOnCityExclude()
	|--------------------------------------------------------------------------
	|
	| Check is user city is exluded for campaign (field city_exclude)
	*/

    public static function isUserOnCityExclude($user_city,$campaign) {

		if((in_array($user_city, explode(",",$campaign->city_exclude)))){

			return true;

		}

	}

	/*
	|--------------------------------------------------------------------------
	| isUserIpCapped()
	|--------------------------------------------------------------------------
	|
	| Checks if user Ip is in memcache for campaigns where ip_cap is enabled
	| Expiry can be 24/48/74 hours
	*/

    public static function isUserIpCapped($campaign) {


		$ip = ip2long($_SERVER['REMOTE_ADDR']);

		if(Cache::has('ipcap-'.$campaign->campaign_id.':'.$ip)){

			return true;

		}else{

			$minutes = $campaign->ip_cap_time / 60;
			Cache::add('ipcap-'.$campaign->campaign_id.':'.$ip, time()+$campaign->ip_cap_time, $minutes);

		}



	}

	/*
	|--------------------------------------------------------------------------
	| isUserIpBlacklisted()
	|--------------------------------------------------------------------------
	|
	| Checks if user ip is added on ip blacklist
	| Ips added via http://acp.adserver3.com/ipblacklist
	*/

    public static function isUserIpBlacklisted() {

        $ip = $_SERVER['REMOTE_ADDR'];

		$ipblacklistArr = Cache::get('laravel:ipblacklist');

		if($ipblacklistArr){

			if (in_array($ip, $ipblacklistArr)) {

				return true;

			}else{

				return false;

			}

		}else{

			return false;

		}

	}

	/*
	|--------------------------------------------------------------------------
	| isCampaignRedirectEnabled()
	|--------------------------------------------------------------------------
	|
	| Check if redirects are enabled. If not check if campaign redirect auto
	| is set to ON and add count to redirect_checkcount (500 ips)
	*/

    public static function isCampaignRedirectEnabled($campaign) {

		//if Mobile redirect is OFF
		if($campaign->mobile_redirects == 1){

			//if auto enable redirect is ON
			if($campaign->redirects_auto == 0){

				//check if we have checked over 500 hits; if so we will turn ON mobile redirects for this campaign; if not over 500 we add one to checkcount
				if($campaign->redirect_checkcount >= 500){

					$campaign = Campaign::find($campaign->campaign_id);
					$campaign->mobile_redirects = 0;
					$campaign->save();

					return true;

				}else{

					$campaign = Campaign::find($campaign->campaign_id);
					$campaign->increment('redirect_checkcount');

					return false;

				}

			}else{

			return false;

			}

		}else{

			return true;

		}

	}

	/*
	|--------------------------------------------------------------------------
	| isUserDesktop()
	|--------------------------------------------------------------------------
	|
	| Checks if user agent matches a desktop machine
	*/

	public static function isUserDesktop() {

		$agent = new Agent();
		if(!$agent->isMobile()){

			return true;

		}

	}

}
