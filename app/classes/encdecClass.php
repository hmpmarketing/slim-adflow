<?php

class crypto{
	
	public static function encrypt_decrypt($action, $string) {
		$output = false;
	
		$encrypt_method = "AES-256-CBC";
		$secret_key = 'smdf0dsdfsdfm1290mhhgdf09m0923hj';
		$secret_iv = 'sdfsdfsdfsdfsdfsf090df';
	
		$key = hash('sha256', $secret_key);
		
		$iv = substr(hash('sha256', $secret_iv), 0, 16);
	
		if( $action == 'encrypt' ) {
			$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
			$output = base64_encode($output);
		}
		else if( $action == 'decrypt' ){
			$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		}
	
		return $output;
	}	
	
	public static function encUrlParam($array){
		
		$urldata = serialize($array);
		$key = pack('H*', "ad");
		$urldata = self::encrypt($urldata);	
		
		return self::base64url_encode($urldata);
		
	}
	
	public static function decUrlParam($urldata){
	
		$urldata = self::base64url_decode($urldata);
		
		$array = unserialize(self::decrypt($urldata));
		
		return $array;
		
	}
	
	
	public static  function encrypt($data) {
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
   	    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		return
				trim( base64_encode( @mcrypt_encrypt(
						MCRYPT_RIJNDAEL_256,
						$key,
						$data,
						MCRYPT_MODE_CBC,
						$iv
				)));
		}
	
	public static  function decrypt($data) {
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
   	    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		return
				@mcrypt_decrypt(
						MCRYPT_RIJNDAEL_256,
						$key,
						base64_decode($data),
						MCRYPT_MODE_CBC,
						$iv
				);
	}
	
	public static function base64url_encode($data) { 
	  return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
	} 
	
	public static function base64url_decode($data) { 
	  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
	} 	

}