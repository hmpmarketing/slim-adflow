<?php

/*
|--------------------------------------------------------------------------
| class statsWrite
|--------------------------------------------------------------------------
|
| Functions related to inserting data into ES and DB
*/

class statsWrite {

	/*
	|--------------------------------------------------------------------------
	| es()
	|--------------------------------------------------------------------------
	|
	| Write data to Redis/Logstash -> ElasticSearch
	*/

  public static function es($data) {
	  	  
		try{

		  $log = array(
			  'campaign_id' => $data->campaign->campaign_id,
			  'supplier_id' => $data->campaign->category,
			  'action_id' => $data->action_id ,
			  'action_code' => $data->action_code,
			  'action_subid' => $data->action_subid,
			  'network_id' => $data->network_id,
			  'zone_id' => $data->zone_id,
			  'user_country' => $data->user_country,
			  'user_city_state' => $data->city_state,
			  'isp_name' => $data->isp_name,
			  'ip_address' => $data->ip_address,
			  'user_agent' => $data->user_agent,
			  'http_referer' => $data->http_referer,
			  'uniqid' => $data->id,
			  'http_host' => $data->http_host,
			  '@timestamp' => date(DATE_ATOM, time()),
			  'geo_block' => $data->geo_block,
			  'isp_block' => $data->isp_block,
			  'city_block' => $data->city_block,
			  'geo_exclude' => $data->geo_exclude,
			  'desktop' => $data->desktop,
			  'redirect' => $data->redirect,
			  'converted' => $data->converted,
			  'ip_capped' => $data->ip_capped,
			  //elasticsearch somehow doesn't accept documents with the field 
			  //isp_blacklisted set to true although the field mapping explicitly says it's a boolean field... 
			  //crazy... we created a different field, blacklisted_ip which works ok. Todo: remove the ip_blacklisted field from our new indexes
			  //tip: remove the field from this array a few minutes before midnight utc and deploy the application. when a new index gets created it won't have the ip_backlisted field anymore
			  'ip_blacklisted' => false,
			  'blacklisted_ip' => $data->blacklisted_ip,
			  'redirect_disabled' => $data->redirect_disabled,
			  'error_flag' => $data->error_flag,
			  'no_action' => $data->no_action,
			  'is_unique' => $data->is_unique,
			  'type' => 'adserver3-stats',
		  );

			$client = new \Predis\Client(array(
				'scheme' => 'tcp',
				'host'   => 'redis.adserver3.com',
				'port'   => 6379,
				'timeout' => 0.3,
			));

  		$client->rPush('logstash', json_encode($log) );
		$client->quit();

		}catch( Exception $e ){

		   return true;

		}


	}


}
