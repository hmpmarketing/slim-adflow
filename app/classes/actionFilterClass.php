<?php

/*
|--------------------------------------------------------------------------
| class actionFilter
|--------------------------------------------------------------------------
|
| Filter checks for mobile actions. It will return a list of action_ids filtered
*/

class actionFilter {


	/*
	|--------------------------------------------------------------------------
	| masterGroupFilter()
	|--------------------------------------------------------------------------
	|
	| Master Group Filter: we now check for a group matching this users geo and
	| campaign id and if group has been enabled
	| if a group exists for this geo and the campaign has been added to it,
	| lets pick the actions added to this group and override all the ones added
	| for the campaign, else, we simply pick the
	| default actions assigned
	*/

    public static function masterGroupFilter($user_country,$campaign_id) {

		$actions = MasterGroup::where('geo',$user_country)
		->where('status',0)
		->whereRaw('campaigns REGEXP "(^|,)('.$campaign_id.')(,|$)"')
		->get(array('actions'));

		if($actions->isEmpty()){

			$actions = IspRedirect::where('campaign_id',$campaign_id)->get();

			return @$actions[0]->mobile_actions;

		}else{

			return $actions[0]->actions;

		}

	}


	/*
	|--------------------------------------------------------------------------
	| geoBlockFilter()
	|--------------------------------------------------------------------------
	|
	| GEO BLOCK TRAFFIC Filter: If traffic is geo block,
	| we set $mobile_actions with the ones added to
	| the master geo block group
	*/

    public static function geoBlockFilter() {

		$actions = MasterGroup::where('geo','ALL')
		->where('status',0)
		->get(array('actions'));

		return $actions[0]->actions;

	}

	/*
	|--------------------------------------------------------------------------
	| geoIspCityFilter() - Get actions that have passed the filter chain:
	| ISP and City, if no match, we try for ISP match
	| If no match we try for actions set as ALL ISPS
	| If no match, then return default empty array
	|--------------------------------------------------------------------------
	*/

	public static function geoIspCityFilter($mobileactions, $country, $city_state, $isp, $deviceinfo) {

		$filtered_actions = array();

		$actions = MobileAction::whereIn('action_id',$mobileactions)
		->whereRaw('isps REGEXP "(^|,)('.$isp.'|ALL ISPS)(,|$)"')
		->whereRaw('geo_cities REGEXP "(^|,)('.$city_state.')(,|$)"')
		->whereRaw('geo_countries REGEXP "(^|,)('.$country.'|ALL)(,|$)"')
		->whereRaw('action_devices REGEXP "(^|,)('.$deviceinfo->device_id.'|1)(,|$)"')
		->whereRaw($deviceinfo->sql)
		->whereRaw("'".date('H:m:s')."' BETWEEN time_from and time_to")
		->where('city_filter',0)
		->where('status',0)
		->get(array('action_id'));

		if($actions->isEmpty()){

			$actions = MobileAction::whereIn('action_id',$mobileactions)
			->whereRaw('isps REGEXP "(^|,)('.$isp.')(,|$)"')
			->whereRaw('geo_countries REGEXP "(^|,)('.$country.'|ALL)(,|$)"')
			->whereRaw('action_devices REGEXP "(^|,)('.$deviceinfo->device_id.'|1)(,|$)"')
			->whereRaw($deviceinfo->sql)
			->whereRaw("'".date('H:m:s')."' BETWEEN time_from and time_to")
			->where('city_filter',1)
			->where('status',0)
			->get(array('action_id'));

			if($actions->isEmpty()){

				$actions = MobileAction::whereIn('action_id',$mobileactions)
				->whereRaw('isps REGEXP "(^|,)(ALL ISPS)(,|$)"')
				->whereRaw('geo_countries REGEXP "(^|,)('.$country.'|ALL)(,|$)"')
				->whereRaw('action_devices REGEXP "(^|,)('.$deviceinfo->device_id.'|1)(,|$)"')
				->whereRaw($deviceinfo->sql)
				->whereRaw("'".date('H:m:s')."' BETWEEN time_from and time_to")
				->where('city_filter',1)
				->where('status',0)
				->get(array('action_id'));

				if($actions->isEmpty()){

					return $filtered_actions;

				}

			}

		}

		foreach($actions as $action){

			$filtered_actions[] = $action->action_id;

		}

		return $filtered_actions;

	}

	/*
	|--------------------------------------------------------------------------
	| getVisited()
	|--------------------------------------------------------------------------
	|
	| Visited Filter: Lets found out if there are any actions on memcache
	| that this user has already seen based on capping time (24/48/72)
	| We then filter these out from $mobileactions array
	*/

	public static function getVisited($mobileactions) {

		$ip = ip2long($_SERVER['REMOTE_ADDR']);

		$i = 0;

		foreach($mobileactions as $action_id){

			if(Cache::has('actioncookiecap-'.$action_id.':'.$ip)){

				$mobileactions = array_diff($mobileactions, array($action_id));

			}

			$i++;

		}

		return $mobileactions;

	}

	/*
	|--------------------------------------------------------------------------
	| setVisited()
	|--------------------------------------------------------------------------
	|
	| Set action cap where action has capping enabled
	| Creates key on memcache that is used by getVisited()
	*/

	public static function setVisited($action) {

	  $ip = ip2long($_SERVER['REMOTE_ADDR']);

	  $minutes = $action[0]->cookie_time / 60;

	  Cache::add('actioncookiecap-'.$action[0]->action_id.':'.$ip, time()+$action[0]->cookie_time, $minutes);

	  return true;

	}


}
