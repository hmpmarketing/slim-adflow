<?php

class RequestInfo {

  public function __construct($campaign, $userGeoInfo) {

	// campaign info
    $this->campaign = $campaign;

    // geo info
    $this->user_country = $userGeoInfo->isoCode;
    $this->city_state = $userGeoInfo->city_state;
    $this->isp_name = $userGeoInfo->isp;

    // required default values that might be set throughout the ad flow
    $this->geo_block = false;
    $this->isp_block = false;
    $this->city_block = false;
    $this->geo_exclude = false;
    $this->desktop = false;
    $this->redirect = false;
    $this->converted = false;
    $this->ip_capped = false;
    $this->blacklisted_ip = false;
    $this->redirect_disabled = false;
    $this->error_flag = 'NO_ERROR';
    $this->no_action = false;
    $this->zone_id = '';
    $this->action_id = '';
    $this->action_code = '';
    $this->action_subid = '';
    $this->network_id = '';

    // static values that won't get touched anymore
    $this->id = str_replace(".", "", uniqid('', true));
    $this->ip_address = $_SERVER['REMOTE_ADDR'];
    $this->user_agent = $_SERVER['HTTP_USER_AGENT'];
    $this->http_referer = @$_SESSION['HTTP_REFERER'];
    $this->http_host = Helpers::getDomain();
    $this->is_unique = uniqueCheck::isUnique($this->campaign->campaign_id);
  }

  function save() {
    return Cache::add($this->id, serialize($this), 1);
  }

  function delete(){
    Cache::forget($this->id);
  }

  public static function get($id) {
    // get key or return false and log messages
    $key = Cache::get($id, false);
    if($key) {
      return unserialize($key);
    }
    else {
      return false;
    }
  }


  /*
  |--------------------------------------------------------------------------
  | filter()
  |--------------------------------------------------------------------------
  |
  | Several campaign filter methods into one call to be used on
  | adDeliveryController and AdJsController
  |
  | sets serveral request attributes
  | returns boolean
  */

  function filter() {

    if(!campaignFilter::isCampaignRedirectEnabled($this->campaign)){
      $this->redirect_disabled = true;
      $this->error_flag = 'REDIRECT_OFF';
      return false;
    }

    if(campaignFilter::isUserOnGeoExclude($this->user_country,$this->campaign)){
      $this->geo_exclude = true;
      $this->error_flag = 'GEO_EXCLUDE';
      return false;
    }

    //Only check this filter if campaign isp whitelist is ON
    if($this->campaign->isp_whitelist == 0){
      if(!campaignFilter::isUserIspWhitelisted($this->user_country,$this->isp_name, $this->campaign)){
        $this->isp_block = true;
        $this->error_flag = 'ISP_BLOCK';
        return false;
      }
    }

    //Only check this filter if campaign isp whitelist is ON
    if($this->campaign->city_exclude != "" || !empty($this->campaign->city_exclude)){
      if(campaignFilter::isUserOnCityExclude($this->city_state,$this->campaign)){
        $this->city_block = true;
        $this->error_flag = 'CITY_BLOCK';
        return false;
      }
    }

    //Only do filter check if campaign ip cap is ON
    if($this->campaign->ip_cap == 0){
      if(campaignFilter::isUserIpCapped($this->campaign)){
        $this->ip_capped = true;
        $this->error_flag = 'IP_CAPPED';
        return false;
      }
    }

    if(campaignFilter::isUserIpBlacklisted()){
      $this->blacklisted_ip = true;
      $this->error_flag = 'IP_BLOCK';
      return false;
    }

    if(campaignFilter::isUserDesktop()){
      $this->desktop = true;
      if ($this->campaign->campaign_vertical == 'Desktop') {
        $this->error_flag = 'POSTVIEW';
      }
      else {
        $this->error_flag = 'DESKTOP';
      }
      return false;
    }

    // geo block traffic which is not on campaign's geo exclude might be recycled.
    // Flow must not stop and therefore we do not set status to false, but we set geo block to true
    if(!campaignFilter::isUserGeoAllowed($this->user_country,$this->campaign)){
      $this->error_flag = 'GEO_BLOCK';
    }

    return true;


  } // end filter function

}
?>
