<?php

use GeoIp2\Database\Reader;

/*
|--------------------------------------------------------------------------
| class GeoIP
|--------------------------------------------------------------------------
|
| GeoIP class for maxmind databases
*/

class GeoIP{
	

	/*
	|--------------------------------------------------------------------------
	| userGeoInfo()
	|--------------------------------------------------------------------------
	|
	| Gather user info from IP Address
	*/	
	
	public static function userGeoInfo($ip = null) {

		if(empty($ip)){
			
			if(getenv('APPLICATION_ENV') == "local"){
				
				//US
				//$ip = '70.209.129.153';
				$ip = '109.145.148.78';	
				$_SERVER['REMOTE_ADDR'] = $ip;		
			
			}else{
			
				$ip = $_SERVER['REMOTE_ADDR'];	
				
			}
		
		}
		
		if(getenv('APPLICATION_ENV') == "local"){
			
			$maxmindCity = new Reader(dirname(__DIR__).'/maxmind/GeoIP2-City.mmdb');
			$maxmindIsp = new Reader(dirname(__DIR__).'/maxmind/GeoIP2-ISP.mmdb');
		
		}
		
		if(getenv('APPLICATION_ENV') == "production"){
		
			$maxmindCity = new Reader('/usr/share/GeoIP/GeoIP2-City.mmdb');
			$maxmindIsp = new Reader('/usr/share/GeoIP/GeoIP2-ISP.mmdb');
		
		}
		
		if(getenv('APPLICATION_ENV') == "staging"){
		
			$maxmindCity = new Reader('/usr/share/GeoIP/GeoIP2-City.mmdb');
			$maxmindIsp = new Reader('/usr/share/GeoIP/GeoIP2-ISP.mmdb');
		
		}	
		
		try {			
		
			$recordCity = $maxmindCity->city($ip);
			
			$recordIsp = $maxmindIsp->isp($ip);
			
			$location = new stdClass;
			$location->ip = $ip;
			$location->isoCode = $recordCity->country->isoCode;
			$location->city_state = $recordCity->city->name.$recordCity->mostSpecificSubdivision->isoCode;
			$location->geo_name_id = $recordCity->city->geonameId;
			$location->isp = $recordIsp->isp;
			
			return $location;	
		
		} catch (GeoIp2\Exception\AddressNotFoundException $e) {
			
			echo 'Invalid IP Address';
			exit;
			$location = new stdClass();
			$location->ip = '';
			$location->isoCode = '';
			$location->city_state = '';
			$location->isp = '';
			$location->geo_name_id = '';
	
			return $location;		
			
		}
		


	}	


}