<?php

class hashStr{
	

	/*
	|--------------------------------------------------------------------------
	| randomAdDeliverystr()
	|--------------------------------------------------------------------------
	|
	| Generate random AdDelivery String
	*/	
	
	public static function randomAdDeliverystr()
	{
		$firstchar = 'abcdefghijklmnopqrstuvwxyz';
		$secondchar = '1234567890';
		$thirdchar = 'abcdefghijklmnopqrstuvwxyz';
		$firstRandString = '';
		$secondRandString = '';
		$thirdRandString = '';
		
		for ($i = 0; $i < rand(2,8); $i++) {
			$firstRandString .= $firstchar[rand(0, strlen($firstchar) - 1)];
		}
		
		for ($i = 0; $i < rand(1,5); $i++) {
			$secondRandString .= $secondchar[rand(0, strlen($secondchar) - 1)];
		}
		
		for ($i = 0; $i < rand(1,4); $i++) {
			$thirdRandString .= $thirdchar[rand(0, strlen($thirdchar) - 1)];
		}		
		
		$randomString = $firstRandString.$secondRandString.$thirdRandString;
		
		return $randomString;
	}
	
	
	/*
	|--------------------------------------------------------------------------
	| randomAdenginestr()
	|--------------------------------------------------------------------------
	|
	| Generate random AdEngine String
	*/	
	
	public static function randomAdenginestr()
	{
		$firstchar = '1234567890';
		$secondchar = 'abcdefghijklmnopqrstuvwxyz';
		$thirdchar = '1234567890';
		$firstRandString = '';
		$secondRandString = '';
		$thirdRandString = '';
		
		for ($i = 0; $i < rand(2,8); $i++) {
			$firstRandString .= $firstchar[rand(0, strlen($firstchar) - 1)];
		}
		
		for ($i = 0; $i < rand(1,5); $i++) {
			$secondRandString .= $secondchar[rand(0, strlen($secondchar) - 1)];
		}
		
		for ($i = 0; $i < rand(1,4); $i++) {
			$thirdRandString .= $thirdchar[rand(0, strlen($thirdchar) - 1)];
		}		
		
		$randomString = $firstRandString.$secondRandString.$thirdRandString;
		
		return $randomString;
	}
	
	public static function randomJSstr(){
		
		$secondchar = 'abcdefghijklmnopqrstuvwxyz';	
		
		$firstRandString = rand(1,9);
	
		for ($i = 0; $i < rand(1,7); $i++) {
			@$secondRandString .= $secondchar[rand(1, strlen($secondchar) - 1)];
		}
	
		$thirdRandString = rand(1,9);	
		
		$randomString = $firstRandString.$secondRandString.$thirdRandString;
		
		return $randomString;	
		
	}	

}