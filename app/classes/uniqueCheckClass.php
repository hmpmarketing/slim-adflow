<?php


/*
|--------------------------------------------------------------------------
| class uniqueCheck
|--------------------------------------------------------------------------
|
| Check if campaign_id:ip exists in memcache to allow unique count from ES
*/

class uniqueCheck {

	/*
	|--------------------------------------------------------------------------
	| isUnique()
	|--------------------------------------------------------------------------
	|
	| Checks if key campaign_id:ip exists in memcache.
	| Returns false if it does. Creates memcache key if it doesnt with
	| expiration @midnight (set expire = $date, which how many minutes/seconds
	| to midnight)
	*/

    public static function isUnique($campaign_id) {

	  $ip = $_SERVER['REMOTE_ADDR'];

	  $unique_key = 'unique-'.$campaign_id.':'.$ip;

	  if(Cache::has($unique_key)){

		  return false;

	  }else{

		 $date = new DateTime;
		 $date->modify('+1 day');
		 $date->setTime(0, 0);

		 Cache::put($unique_key, '', $date);

		 return true;

	  }

	}

}
