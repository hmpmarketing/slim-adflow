<?php

use Illuminate\Cache\CacheManager as CacheManager;

class Cache extends CacheManager {
	
	public static function cacheManager() 
    {
	
		$cacheConfig = array(
			'config' => array(
				'cache.driver' => 'memcached',
				'cache.connection' => null,
				'cache.memcached' => array(
					array('host' => 'mysql.adserver3.com', 'port' => 11211, 'weight' => 100),
				),
		
				'cache.prefix' => '',
			),
			'memcached.connector' => new \Illuminate\Cache\MemcachedConnector(),
		);
		 
		$cacheManager = new CacheManager($cacheConfig);
		$cache = $cacheManager->driver();
		return $cache;
		
	}
	
	
	public static function has($key){
	
		return Cache::cacheManager()->has($key);
		
	}
	
	
	public static function put($key,$val,$time){
		
		return Cache::cacheManager()->put($key,$val,$time);
		
	}
	
	
	public static function get($key){
		
		return Cache::cacheManager()->get($key);
		
		
	}
	
	public static function add($key, $value, $minutes){
		
		return Cache::cacheManager()->add($key, $value, $minutes);
		
	}
	
	public static function forget($key){
		
		return Cache::cacheManager()->forget($key);
		
	}		
	
	
}