<?php

use Philo\Blade\Blade;

/*
|--------------------------------------------------------------------------
| class view
|--------------------------------------------------------------------------
|
| Generate user views for banner, postview or redirect html code
*/

class render {
	
	public static function newBlade(){
	
		$viewdir = dirname(__DIR__) . '/views';	
		$cachedir = dirname(__DIR__) . '/cache';
	
		$blade = new Blade($viewdir, $cachedir);
		return $blade;	
		
	}

	/*
	|--------------------------------------------------------------------------
	| generateJSAdDeliveryIframe()
	|--------------------------------------------------------------------------
	|
	| Generate JS Code that renders adDelivery Iframe when user PASS filter
	*/	
	
	public static function generateJSAdDeliveryIframe($campaign,$hash){
		
		$protocol = Helpers::getRequestProtocol();
		$domain = Helpers::getDomain();
		$randomaddeliverystring = hashStr::randomAdDeliverystr();
		$bannerDimensions = Helpers::getBannerSize($campaign->campaign_dimensions);
		
		//Random strings for js elements
		$randstdivname = hashStr::randomAdenginestr();
		$randstdivimgname = hashStr::randomAdenginestr();
		$randifrmname = hashStr::randomAdenginestr();
		$randbannerdiv = hashStr::randomAdenginestr();
		
		$gostatscode = '';
		if($campaign->campaign_id ==3447 || $campaign->campaign_id ==3448 || $campaign->campaign_id ==3449){
		
			$gostatscode = "var img=document.createElement('img');img.src='http://c4.gostats.com/bin/count/a_401245/t_5/i_1/counter.png';img.width='1';img.height='1';document.body.appendChild(img);";	
			
		}
		
		if($campaign->campaign_id ==3839){
		
			$gostatscode = "var img=document.createElement('img');img.src='http://c4.gostats.com/bin/count/a_401246/t_5/i_1/counter.png';img.width='1';img.height='1';document.body.appendChild(img);";	
			
		}	
		
		$blade = render::newBlade();	
		
		$view = $blade->view()->make('jsIframeCode')
		->with('protocol',$protocol)
		->with('domain',$domain)
		->with('randomaddeliverystring',$randomaddeliverystring)
		->with('bannerDimensions',$bannerDimensions)
		->with('hash',$hash)
		->with('randstdivname',$randstdivname)
		->with('randstdivimgname',$randstdivimgname)
		->with('randifrmname',$randifrmname)
		->with('randbannerdiv',$randbannerdiv)
		->with('gostatscode',$gostatscode);
		
		//Remove <script> tags from template
		$view = str_replace("<script>","",$view);
		$view = str_replace("</script>","",$view);	
		
		echo $view;
		exit;	
		
	}
	
	/*
	|--------------------------------------------------------------------------
	| generateAdEngineIframe()
	|--------------------------------------------------------------------------
	|
	| Generate HTML code that renders adEngine Iframe
	*/	
	
	public static function generateAdEngineIframe($campaign,$hash){
		
		$protocol = Helpers::getRequestProtocol();
		$domain = Helpers::getDomain();
		$randomadenginestring = hashStr::randomAdenginestr();
		$bannerDimensions = Helpers::getBannerSize($campaign->campaign_dimensions);
		
		$blade = render::newBlade();	
		
		$view = $blade->view()->make('adEngineIframe')
		->with('protocol',$protocol)
		->with('domain',$domain)
		->with('randomadenginestring',$randomadenginestring)
		->with('bannerDimensions',$bannerDimensions)
		->with('hash',$hash);	
		
		echo $view;
		exit;	
		
	}	

	/*
	|--------------------------------------------------------------------------
	| generateBannerAd()
	|--------------------------------------------------------------------------
	|
	| Generate banner ad for a campaign when user is blocked by filter
	*/	

	public static function generateBannerAd($campaign,$error) {
		
		$bannerDimensions = Helpers::getBannerSize($campaign->campaign_dimensions);
		$creative = Creative::whereIn('creative_id',explode(",",$campaign->creatives))
		->where('width',$bannerDimensions->width)
		->where('height',$bannerDimensions->height)
		->where('creative_audience',$campaign->campaign_audience)
		->take(1)
		->get();
				
		$blade = render::newBlade();	
		
		$view = $blade->view()->make('bannerAd')
		->with('creative',$creative)
		->with('error',$error)
		->with('cdn',Helpers::getCDN());
		
		echo $view;
		exit;
	
					
	}
	

	/*
	|--------------------------------------------------------------------------
	| generateJSBannerAd()
	|--------------------------------------------------------------------------
	|
	| Generate JS based banner ad for a campaign when user is blocked by filter
	*/	

	public static function generateJSBannerAd($campaign,$error) {
		
		$bannerDimensions = Helpers::getBannerSize($campaign->campaign_dimensions);
		$creative = Creative::whereIn('creative_id',explode(",",$campaign->creatives))
		->where('width',$bannerDimensions->width)
		->where('height',$bannerDimensions->height)
		->where('creative_audience',$campaign->campaign_audience)
		->take(1)
		->get();
		
		//Rand strings for js elements
		$randanchorname = 'a'.hashStr::randomAdenginestr();	
		$randimgname = 'i'.hashStr::randomAdenginestr();	
		$anchorurlname = 'u'.hashStr::randomAdenginestr();
		$cachebuster = mt_rand();		
		
		$blade = render::newBlade();	
		
		$view = $blade->view()->make('jsBannerCode')
		->with('creative',$creative)
		->with('error',$error)
		->with('cdn',Helpers::getCDN())
		->with('randanchorname',$randanchorname)
		->with('randimgname',$randimgname)
		->with('anchorurlname',$anchorurlname)
		->with('cachebuster',$cachebuster);
		
		//Remove <script> tags from template
		$view = str_replace("<script>","",$view);
		$view = str_replace("</script>","",$view);		
		
		echo $view;
		exit;
	
					
	}	
	
	/*
	|--------------------------------------------------------------------------
	| generateRedirectCode()
	|--------------------------------------------------------------------------
	|
	| Generate redirect page
	*/	
	
	public static function generateRedirectCode($url) {

		$protocol = Helpers::getRequestProtocol();
		$domain = Helpers::getDomain();
				
		$blade = render::newBlade();	
		
		$view = $blade->view()->make('redirector')
		->with('protocol',$protocol)
		->with('domain',$domain)		
		->with('url',$url);
		
		echo $view;
		exit;			
	}
	
	
	/*
	|--------------------------------------------------------------------------
	| generatePopupCode()
	|--------------------------------------------------------------------------
	|
	| Generate Popup page (for popup actions)
	*/	
	
	public static function generatePopupCode($url,$popup_message) {

		$protocol = Helpers::getRequestProtocol();
		$domain = Helpers::getDomain();
		
		$blade = render::newBlade();	
		
		$view = $blade->view()->make('popup')
		->with('protocol',$protocol)
		->with('domain',$domain)		
		->with('url',$url)
		->with('popup_message',$popup_message);
		
		echo $view;
		exit;			
	}		

	/*
	|--------------------------------------------------------------------------
	| generatePostView()
	|--------------------------------------------------------------------------
	|
	| Generate banner ad + cookie drop for a Desktop campaign
	*/

	public static function generatePostView($campaign) {
		
		$bannerDimensions = Helpers::getBannerSize($campaign->campaign_dimensions);
		
		$creatives = Creative::whereIn('creative_id',explode(",",$campaign->creatives))
		->where('width',$bannerDimensions->width)
		->where('height',$bannerDimensions->height)
		->where('creative_audience',$campaign->campaign_audience)
		->get();
		
		//print_r($creatives);exit;
		
		$protocol = Helpers::getRequestProtocol();
		$domain = Helpers::getDomain();
		
		$blade = render::newBlade();	
		
		$view = $blade->view()->make('pvBannerAd')
		->with('protocol',$protocol)
		->with('domain',$domain)
		->with('cdn',Helpers::getCDN())		
		->with('bannerDimensions',$bannerDimensions)
		->with('creatives',$creatives);
		
		echo $view;
		exit;				
					
	}
	
	/*
	|--------------------------------------------------------------------------
	| generateJSPostView()
	|--------------------------------------------------------------------------
	|
	| Generate iframe JS banner ad + cookie drop for a Desktop campaign
	*/

	public static function generateJSPostView($campaign) {
		
		$bannerDimensions = Helpers::getBannerSize($campaign->campaign_dimensions);
		$protocol = Helpers::getRequestProtocol();
		$domain = Helpers::getDomain();		
		
		//Random strings for js elements
		$randstdivname = hashStr::randomAdenginestr();
		$randstdivimgname = hashStr::randomAdenginestr();
		$randifrmname = hashStr::randomAdenginestr();
		$randbannerdiv = hashStr::randomAdenginestr();		
		
		$blade = render::newBlade();	
		
		$view = $blade->view()->make('jsPvIframeCode')
		->with('bannerDimensions',$bannerDimensions)
		->with('protocol',$protocol)
		->with('domain',$domain)
		->with('randstdivname',$randstdivname)
		->with('randstdivimgname',$randstdivimgname)
		->with('randifrmname',$randifrmname)
		->with('randbannerdiv',$randbannerdiv)		
		->with('campaign',$campaign);
		
		//Remove <script> tags from template
		$view = str_replace("<script>","",$view);
		$view = str_replace("</script>","",$view);		
		
		echo $view;
		exit;				
					
	}	
	
	public static function jQuery() {
	
		$blade = render::newBlade();	
		
		$view = $blade->view()->make('jquery');
		echo $view;	
		
	}
	
	
}