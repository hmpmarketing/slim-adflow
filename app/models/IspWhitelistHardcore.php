<?

use Illuminate\Database\Eloquent\Model as Eloquent;

class IspWhitelistHardcore extends Eloquent
{	
	protected $table = 'isp_whitelist_hardcore';
	public $timestamps = false;

}