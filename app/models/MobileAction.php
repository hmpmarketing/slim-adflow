<?

use Illuminate\Database\Eloquent\Model as Eloquent;

class MobileAction extends Eloquent
{
	
	protected $table = 'mobile_actions';
	public $timestamps = false;
	protected $primaryKey = 'action_id';

}