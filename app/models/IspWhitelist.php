<?

use Illuminate\Database\Eloquent\Model as Eloquent;

class IspWhitelist extends Eloquent
{
	
	protected $table = 'isp_whitelist';
	public $timestamps = false;

}