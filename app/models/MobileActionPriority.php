<?

use Illuminate\Database\Eloquent\Model as Eloquent;

class MobileActionPriority extends Eloquent
{
	
	protected $table = 'mobile_actions_priority';
	public $timestamps = false;
	protected $primaryKey = 'id';
	

}