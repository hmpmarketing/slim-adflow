<?

use Illuminate\Database\Eloquent\Model as Eloquent;

class Creative extends Eloquent
{
	
	protected $table = 'creatives';
	public $timestamps = false;
	protected $primaryKey = 'creative_id';

}