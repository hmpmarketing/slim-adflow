<?

use Illuminate\Database\Eloquent\Model as Eloquent;

class MobileLead extends Eloquent
{
	
	protected $table = 'mobile_leads';
	public $timestamps = false;

}