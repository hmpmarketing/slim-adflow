<?

use Illuminate\Database\Eloquent\Model as Eloquent;

class Network extends Eloquent
{
	
	protected $table = 'networks';
	public $timestamps = false;
	protected $primaryKey = 'network_id';

}