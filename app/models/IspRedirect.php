<?

use Illuminate\Database\Eloquent\Model as Eloquent;

class IspRedirect extends Eloquent
{
	
	protected $table = 'isp_redirects';
	public $timestamps = false;
	protected $primaryKey = 'campaign_id';

}