<?

use Illuminate\Database\Eloquent\Model as Eloquent;

class campaign extends Eloquent
{	
	
	protected $table = 'campaigns';
	public $timestamps = false;
	protected $primaryKey = 'campaign_id';

}