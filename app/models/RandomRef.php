<?

use Illuminate\Database\Eloquent\Model as Eloquent;

class RandomRef extends Eloquent
{
	
	protected $table = 'random_refs';
	public $timestamps = false;

}