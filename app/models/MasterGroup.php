<?php

use Illuminate\Database\Eloquent\Model as Eloquent;


class MasterGroup extends Eloquent
{
	
	protected $table = 'campaign_groups';
	public $timestamps = false;

}