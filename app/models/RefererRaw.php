<?

use Illuminate\Database\Eloquent\Model as Eloquent;

class RefererRaw extends Eloquent
{
	
	protected $table = 'referer_raw';
	public $timestamps = false;

}