<?php

use Illuminate\Database\Capsule\Manager as Capsule;
$capsule = new Capsule;
$capsule->addConnection([
			'driver'    => 'mysql',
			'host'      => '192.168.26.4',
			'database'  => 'adserver3',
			'username'  => 'root',
			'password'  => '',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();