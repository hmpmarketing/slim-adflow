<html>
<head>
<title>PV</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style>
body{
	font-family: "Open Sans", helvetica, arial;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;	
}
p.pvsldr-caption{
	background: rgba(255,255,255,0.5);
	font-size:11px;
}
ul.pvsldr{position:relative; list-style:none;padding:0;margin:0;overflow:hidden; display:none;}
li.pvsldr-slide{position:absolute; display:none;}
ul.pvsldr-controls{list-style:none;margin:0;padding:0;z-index:9999;}
ul.pvsldr-controls.v-centered li a{position:absolute;}
ul.pvsldr-controls.v-centered li.pvsldr-next a{right:0;}
ul.pvsldr-controls.v-centered li.pvsldr-prev a{left:0;}
ol.pvsldr-markers{list-style: none; padding: 0; margin: 0; width:100%;}
ol.pvsldr-markers.h-centered{text-align: center;}
ol.pvsldr-markers li{display:inline;}
ol.pvsldr-markers li a{display:inline-block;}
p.pvsldr-caption{display:block;width:96%;margin:0;padding:2%;position:absolute;bottom:0;}
</style>
<script src="{{$protocol}}{{$domain}}/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="{{$protocol}}{{$domain}}/js/slider.js" type="text/javascript"></script>
<script>
jQuery(document).ready(function($) {
	$('#pvsldr').pvsldr({
		'height' : '{{$bannerDimensions->height}}',
		'width' : '{{$bannerDimensions->width}}',
		usecaptions : true,
		responsive : true,
		showcontrols : false,
		centercontrols : false,
		showmarkers : false,
		centermarkers : false,
	});
});
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div id="pvsldr">
	<ul class="pvsldr">
    <?php $pvimgs = ''; ?>
    @foreach($creatives as $creative)
	<?php
			if($creative->alternate_url != ""){
			
				$creative->creative_url = $creative->alternate_url;
			  
			}
			
			$pvimgs .= '<img style="display:none;" src="'.$creative->creative_url.'"/>';
	
	?>
		<li><a href="{{$creative->creative_url}}"><img id="{{$creative->creative_id}}" title="{{$creative->creative_caption}}" src="http://cdn.adserver3.com/{{$creative->creative_image}}"/></a></li>
	@endforeach
    </ul>
</div>
{{$pvimgs}}
</body>
</html>

