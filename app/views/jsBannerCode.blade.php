<script>
//{{base64_encode(rand().'-'.$error)}}//
window.onload = function() {
    if (typeof clicktag !== "undefined") {
        {{$randanchorname}} = clicktag;
    } else {
        {{$randanchorname}} = "{{$creative[0]->creative_url}}";
    }
    if (typeof cachebuster !== "undefined") {
        cachebusterrand = cachebuster + "{{$cachebuster}}";
    } else {
        cachebusterrand = "{{$cachebuster}}";
    }
    var {{$randimgname}} = document.createElement('a');
    {{$randimgname}}.href = {{$randanchorname}};
    var {{$anchorurlname}} = document.createElement('img');
    {{$randimgname}}.target = '_blank';
    {{$anchorurlname}}.setAttribute('src', '{{$cdn}}/{{$creative[0]->creative_image}}?rnd=' + cachebusterrand + '&data=hash');
    {{$anchorurlname}}.setAttribute('alt', '');
    if (typeof pixelborder == 'undefined') {
        pixelborder = 0;
    }
    {{$anchorurlname}}.setAttribute('border', pixelborder);
    {{$randimgname}}.appendChild({{$anchorurlname}});
    document.body.style.marginTop = '0px';
    document.body.style.marginLeft = '0px';
    document.body.appendChild({{$randimgname}});
};
</script>