<?php

require '../vendor/autoload.php';
require '../vendor/illuminate/support/illuminate/support/helpers.php';

//Register  autoloader
Illuminate\Support\ClassLoader::register();

$basePath = str_finish(dirname(__DIR__), '/app/');
$env = getenv('APPLICATION_ENV') === 'local' ? 'local' : 'production';
$configPath = $basePath . 'config/'.$env;

$classes = $basePath . 'classes';
$controllers = $basePath . 'controllers';
$models = $basePath . 'models';

Illuminate\Support\ClassLoader::addDirectories(array($classes, $controllers, $models));

$app = new Illuminate\Container\Container();
$app->bind('app', $app);

require $configPath.'/database.php';

$app->bind('path.base', $basePath);
$app->bind('path.classes', $classes);
$app->bind('path.controllers', $controllers);
$app->bind('path.models', $models);

Illuminate\Support\Facades\Facade::setFacadeApplication($app);

$app['app'] = $app;
$app['env'] = $env;

with(new Illuminate\Events\EventServiceProvider($app))->register();
with(new Illuminate\Routing\RoutingServiceProvider($app))->register();

require $basePath.'/routes.php';

$request = Illuminate\Http\Request::createFromGlobals();
$response = $app['router']->dispatch($request);

$response->send();